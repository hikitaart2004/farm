let groundTypes = {
    'gt': 'sprites/ground-top.png',
    'gb': 'sprites/ground-bottom.png'
};
function createMap(gridMap, lenY, lenX){
    let tiles = new Array();
    for (let i = 0; i < lenY; i++){
        for (let j = 0; j < lenX; j++){
            if (gridMap[i][j] === '-'){
                continue;
            }
            else{
                tiles.push(new Tile(groundTypes[gridMap[i][j]], j*32,i*32));
            }
        }
    }
    return tiles;
}
function drawMap(map, len, context){
    for (let i = 0; i < len; i++){
        map[i].drawMe(context);
    }
}
function clearScreen(context){
    context.fillRect(0,0,850,450);
}
function moveCursor(canv, map, len, cursor, cursorX, cursorY){
    //функция работает неправильно. Сильно пролагивает при перемещении мыши.
    //по идеи это решение подходит больше всего, чтобы курсор передвигался только по земле.
    /*for (let i = 0; i < len; i++){
        let x = Math.floor((cursorX - canv.left) / 32)*32;
        let y = Math.floor((cursorY - canv.top) / 32)*32;
        let xCell = map[i].getPos()[0];
        let yCell = map[i].getPos()[1];
        if (xCell === x && yCell == y){
            if (cursor.getPos()[0] !== x && cursor.getPos()[1] !== y){
                cursor.changePos(x,y);
            }
        }
    }*/
    let x = Math.floor((cursorX - canv.left) / 32)*32;
    let y = Math.floor((cursorY - canv.top) / 32)*32;
    cursor.changePos(x,y);
}

