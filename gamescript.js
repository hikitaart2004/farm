let tileMap = new Array();
let gridMap = 
[['-','-','-','-','-','-','-','-','-','-','-','-','-'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gt','gt','gt','gt','gt','gt','gt','gt'],
['-','-','-','-','-','gb','gb','gb','gb','gb','gb','gb','gb']];

let cursor = new Tile('sprites/farm-border.png', 0, 0);
let canvas = document.getElementsByTagName('canvas')[0];
canvas.onmousemove = function(event){
    moveCursor(canvas.getBoundingClientRect(),tileMap, tileMap.length, cursor, event.clientX, event.clientY);
}
let ctx = canvas.getContext('2d');
ctx.fillStyle = 'lightblue';

tileMap = createMap(gridMap, gridMap.length, gridMap[0].length);
setInterval(function() {
clearScreen(ctx);
drawMap(tileMap, tileMap.length, ctx);
cursor.drawMe(ctx);
}, 100);

