class Tile {
    constructor(path, x, y){
        this.img = new Image();
        this.img.src = path;
        this.x = x;
        this.y = y;
    }
    drawMe(canvas){
        canvas.drawImage(this.img, this.x, this.y, 32, 32);
    }
    getPos(){
        return [this.x, this.y];
    }
    changePos(x, y){
        this.x = x;
        this.y = y;
    }
}